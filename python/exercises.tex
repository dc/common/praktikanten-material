\documentclass[10pt,a4paper]{letter}
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\usepackage{stix}
\usepackage{enumitem}
\usepackage{bm}
\usepackage[margin=4.2cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{relsize}
\usepackage{listings}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\rhead{\thepage}
\lhead{Python Exercises for Trainees}

\newcommand{\Hint}[1]{{\\ \color{Blue} Hint:} #1}
\newcommand{\HintN}[2]{{\\ \color{Blue} Hint #1:} #2}
\newcommand{\Note}[1]{{\\ \color{Blue} Note:} #1}
\newcommand{\Example}[1]{{\\ \color{Blue} Example:} #1}

\newcounter{ChapterCounter}
\newcommand{\Chapter}[1]{\refstepcounter{ChapterCounter}\textbf{\theChapterCounter.   #1} \label{#1}}
\newcommand{\ChapterNoNum}[1]{\begin{center}\textbf{#1}\end{center} \label{#1}}
\renewcommand\labelitemi{-}

% for item labels prefixed with an asterisk
\newcommand{\sitem}[1][black]{\stepcounter{enumi}\item[\color{#1}$\bm{*}$\,\textbf{\theenumi}]}


\begin{document}
\lstset{language=Python, basicstyle=\small\ttfamily, showstringspaces=false}

\noindent\fbox{%
    \parbox{\textwidth}{%
\begin{itemize}
    \item[$\rightarrow$] Hints and notes refer to Python 3.
    \item[$\rightarrow$] Difficult exercises are marked with an asterisk, indicating that they can be at first omitted or simply skipped if overchallenging.
\end{itemize}
    }%
}

\ChapterNoNum{Preliminaries}
Composing, formatting, printing strings. Reading values from the prompt into a variable. Importing a module. Checking the type of a variable.

\Chapter{Strings}
\begin{enumerate}[label=\textbf{\alph*)}]
    \item In an interactive Python shell make sure you can:
        \begin{itemize}
            \item concatenate two or more strings into a single one
            \item ``multiply'' a string \texttt{n} times
            \item make a string upper (lower) case
            \item find a substring inside a string
            \item extract from a string a character at a given position
            \item \textit{slice} a string, i.e. extract part of the string, say from position \texttt{m} to position \texttt{n}
        \end{itemize}
\end{enumerate}

\Chapter{Reading values from the prompt - Formatting strings}
\begin{enumerate}[label=\textbf{\alph*)}]
    \item Write a Hello World program, that is a program that just writes: \lstinline{"Hello World!"}
    \item Write a program which asks the user its name and birth year and then writes the following text: \lstinline{"Hi NAME, you will become X years old this year."}.
        \HintN{1}{In Python 3 the \texttt{input} function takes as argument a string to be prompted to the user and return the user's input string. Cast it if appropriate.}
        \HintN{2}{To get the current year and avoid to hardcode it, you should use the appropriate function from the \texttt{datetime} module and therefore \texttt{import} it.}
        %\HintN{3}{To format variables into a string there are various methods. You can try and then choose one of the following:
        \\{\color{Blue} Hint 3:} To format variables into a string there are various methods. You can try and then choose one of the following:
                \begin{lstlisting}[language=Python]
f"var1: {var1}    var2: {var2}"
"var1: {0:8s}    var2: {1:4.2f}".format(var1, var2)
"var1: %s    var2: %d" % (var1, var2)
                \end{lstlisting}
        %}
\end{enumerate}


\vspace{0.5cm}
\ChapterNoNum{Data structures}

Make sure you know the main characteristics of \texttt{list}s, \texttt{tuple}s, \texttt{set}s, \texttt{dict}ionaries.

\Chapter{Lists}
\begin{enumerate}[label=\textbf{\alph*)}]
    \item Since in Python a string is a list of chars, you already know something about lists. Make sure you also can:
    \begin{itemize}
        \item append an element to a list
        \item add the elements of a list to another list
        \item insert/remove an element at a given position
        \item return the last element removing it from the list
        \item sort and revert the order of elements in the list
    \end{itemize}
\item Which methods of the list above would help you using a list as a \textit{stack} (or \textit{LIFO}: Last In First Out)?
\sitem Lists are not good to be used as a \textit{queue} (or \textit{FIFO}: First In First Out). Why?
        \Note{For that purpose you can use \texttt{collections.deque}, which provides a \texttt{popleft} method.}
    \sitem Write a program that fills a list with ten integers taken as input from the terminal and returns only the even ones.
        \Hint{Use a \textit{list comprehension}.}
    \item Write a program which returns in reversed order the words of a sentence received as input.
        \Hint{Use the ``\textit{extended slice syntax}'' \texttt{[::-1]}. Alternatively use the \texttt{reverse} function of list objects.}
        \Example{``\texttt{I like this color}'' should give ``\texttt{color this like I}''.}
    \item \label{rsort} Write a program which returns in alphabetical order the words of a sentence received as input.
        \Example{The previous example should give ``\lstinline{I color like this}''.}
    \sitem \label{irsort} Like in the previous exercise (\ref{Lists}.\ref{rsort}) but case insensitive. \Hint{You can pass the \texttt{str.lower} function as ``\texttt{key=}'' argument of \texttt{sort} or \texttt{sorted}.}
        \Example{The previous example should give ``\texttt{color I like this}''.}
\end{enumerate}

\Chapter{Dictionaries}
\begin{enumerate}[label=\textbf{\alph*)}]
    \item Make sure you can:
    \begin{itemize}
        \item instantiate a dictionary (with \texttt{\{\}} and with \texttt{dict()})
        \item add/remove an entry (key-value pair) to/from a dictionary
        \item get the value associated to a given key
        \item check if the dictionary contains the pair with a given key
        \item iterate over the elements of the dictionary
    \end{itemize}
    \item \label{birthmonth} Write a program that fills a dictionary with ten (name - birthday month) key-value pairs requested as input and prints out the month of birth for the names chosen by the user.
\end{enumerate}

\vspace{0.5cm}
\ChapterNoNum{Control flow}
Keywords like \texttt{if}, \texttt{else}, \texttt{while}, \texttt{for}, \texttt{pass}, \texttt{break}, \ldots allow to shape the flow of statements to be executed.

\Chapter{Conditions and Loops}
\begin{enumerate}[label=\textbf{\alph*)}]
    \item Make sure you can correctly use \texttt{if-elif-else}-statements
    \item Make sure you can correctly use \texttt{for}- and \texttt{while}-loops, in particular the construct ``\texttt{for i in}'' plus \texttt{range()} or plus an iterable.
    \item List the arguments passed to your script.
        \Hint{Using the \texttt{sys} module, \texttt{sys.argv} is a list containing the arguments.}
    \item Write a ``guess the number'' game: the user guesses an integer between 1 and 10 until (s)he enters \texttt{exit} or until (s)he gets the right number.
\end{enumerate}

\Chapter{File I/O}
\begin{enumerate}[label=\textbf{\alph*)}]
    \item Write a program reading a text from an input file and writing it to an output file after replacing newlines with spaces. The two file names are to be given as input.
        \sitem Save a dictionary (e.g. the one from \ref{Dictionaries}.\ref{birthmonth}) to a json file and write a program that fills a dictionary with key-value pairs read from the first json file and writes out the dictionary to a second json file after having added or replaced a few entries according to input given by the user.
        \Hint{Use the \texttt{dump} and \texttt{load} functions provided by the \texttt{json} module.}
\end{enumerate}

\vspace{0.5cm}
\ChapterNoNum{Functions and Modules}

The exercises in this unit ask you to define functions -- blocks of callable code -- and to write and import modules -- files containing Python code which can be imported and utilised by other Python code.

\Chapter{Functions}
\begin{enumerate}[label=\textbf{\alph*)}]
    \item 
        \begin{minipage}[t]{0.7\textwidth}
        Write a program which prints out a chessboard (like in the figure on the right). Define a function \texttt{odd\_row()} and a function \texttt{even\_row} returning a row starting with a white or black square respectively and in the main part of your script call \texttt{odd\_row()} for all odd lines (first and last included) and \texttt{even\_row} for all the lines in between.
        \Hint{Let the line be a string composed of the Unicode characters: \texttt{2B1B} for a black square and \texttt{2B1C} for a white square.}
        \end{minipage}
        \begin{minipage}[t]{0.3\textwidth}
$\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare$\\
$\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare$\\
$\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare$\\
$\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare$\\
$\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare$\\
$\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare$\\
$\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare$\\
$\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare\lgblksquare\lgwhtsquare$
        \end{minipage}
        \sitem Write a \textit{docstring} for both of this functions and verify it gets called when it should (in an interactive Python shell) or call it explicitly with \texttt{.\_\_doc\_\_}
    \sitem Like in exercise \ref{Lists}.\ref{irsort} but this time you are supposed to pass a \textit{lambda function} as ``\texttt{key=}'' argument of \texttt{sort} or \texttt{sorted}, using the \texttt{lower} function of a string object.
        \Example{``\texttt{I like this color}'' should give ``\texttt{color I like this}''}
    \sitem Make a minimal example showing the effect of defining a variable inside a function with or without the keyword \texttt{global}.
    \sitem Write a function taking one positional argument plus unnamed and named arguments and call it with three unnamed and two named arguments.
        \Hint{Use the standard syntax \lstinline{f(a, *args, **kwargs)} and inside the function access the unnamed arguments from the tuple \texttt{args} and the named ones from the dictionary \texttt{kwargs}.}
\end{enumerate}


\Chapter{Modules}
\begin{enumerate}[label=\textbf{\alph*)}]
    \item Create a module for alphabetical sorting (a file named \texttt{alphasort.py}) containing the two functions \texttt{sort} and \texttt{isort}: they take as argument a string, preferably made up of many words, and return a list of words alphabetically sorted, case insensitive in the case of the \texttt{isort} function (use the code from your own solutions of exercises \ref{Lists}.\ref{rsort} and \ref{Lists}.\ref{irsort}).
        \item From a script or an interactive shell import the module in the following ways:
            \begin{enumerate}[label=\textbf{\roman*)}]
                \item \texttt{import alphasort}
                \item \texttt{import alphasort as srt}
                \item \texttt{from alphasort import *}
            \end{enumerate}
            and make use of the two functions.
    \sitem Complete the module with a \texttt{if \_\_name\_\_ == "\_\_main\_\_":} block so that the functions are executed when executing the module but not when loading it.
    \item Use the \texttt{dir} function to inspect the name space of the imported module.
    % \sitem Reshape the module above into a package (modules inside a common directory) and use the \lstinline{__init__.py} file to define which modules are to be exported (the so called \textit{API} of the package) and which not (the internal modules).
\end{enumerate}


\Chapter{Exception handling}
Write a small Python script -- \texttt{inverse.py} -- (or a function -- \texttt{inverse} -- in an interactive shell) which reads a number from the text file given as argument and outputs its inverse.
\begin{enumerate}[label=\textbf{\alph*)}]
    \item Run your script passing first a non-existent file and then a file containing a $0$-charachter. Make sure you understand what happens in both cases.
    \item Modify the script so that it does not terminate when given a non-existent file as argument but let it print an error message instead.
    \sitem In addition to the previous point, your program should not terminate but print out a different error statement
            for the different possible error cases (trigger them by varying the content of the file or the argument passed to the script).
    \sitem Add an action to be executed in case an error occured, which were not catched by previous \texttt{except} statements.
    \sitem Add an action to be executed at any execution of the try statement.
\end{enumerate}
 

\vspace{0.5cm}
\ChapterNoNum{Object Oriented Programming}


\Chapter{Classes}
\begin{enumerate}[label=\textbf{\alph*)}]
        \item \label{fibo} Write a class to implement a Fibonacci iterator. Assuming your class is called \texttt{fib\_iter}, the following \texttt{for}-loop:
        \begin{lstlisting}
for i in fib_iter(500):
    print(i, end=' ')
        \end{lstlisting}
        should return all Fibonacci numbers up to \texttt{500}:
        \begin{lstlisting}
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377
        \end{lstlisting}
        {\color{Blue}Theory: }To conform to the paradigm of the Python iterator, the class needs
        to implement an \texttt{\_\_iter\_\_} method (usully returning \texttt{self}) and a
        \texttt{\_\_next\_\_} method, which defines what is returned at each iteration and when
        the loop should be exited by raising a \texttt{StopIteration} exception. The
        \texttt{for} loop creates an iterator by calling behind the scenes the \texttt{\_\_iter\_\_} method
        and then calles the \texttt{\_\_next\_\_} method at each iteration.
    \sitem Instantiate an object of \texttt{fib\_iter} and display the value of its members after
        manually calling \texttt{object.\_\_next\_\_()}.
    \sitem Add a \textit{docstring} to the class and its methods.
        \sitem Similiarly to what seen for an iterator class in \ref{Classes}.\ref{fibo}, you can create your own \textit{collection} class by following the collection protocol.
        Create a class representing a collection of files in a directory in form of a dictionary, where keys are the filenames of the files contained in the directory and values are the corresponding contents.\\
        %http://zesty.ca/bc/explore-11.html
        {\color{Blue}Theory: }To conform to the collection protocol define the following methods:
        \begin{center}
            \begin{tabular}{c | c}
                method & used by \\ \hline
                \texttt{x.\_\_len\_\_()} & \texttt{len(x)} \\
                \texttt{x.\_\_getitem\_\_(i)} &  \texttt{x[i]}  \\     
                \texttt{x.\_\_setitem\_\_(i, y)} & \texttt{x[i] = y} \\   
                \texttt{x.\_\_delitem\_\_(i)} & \texttt{del x[i]} \\
                \texttt{x.\_\_contains\_\_(y)} & \texttt{y in x}
            \end{tabular}
        \end{center}
\end{enumerate}


\vspace{0.5cm}
\ChapterNoNum{Libraries}
Python provides a very rich standard library and a huge set of other libraries for specific uses. These are usually available as packages which you might need to explicitly install on your system.
For the exercises below, for example, you are supposed to use the \texttt{matplotlib} library for plotting and producing figures and the \texttt{numpy} library for generating functions and dealing with arrays.

\Chapter{Plots and Histograms}
\begin{enumerate}[label=\textbf{\alph*)}]
    \item Plot the functions $sin(x)$ and $cos(x)$ in the range $[0,4]$ .
        \Hint{Use the \texttt{sin} and \texttt{cos} functions for the $y$-axis and the \texttt{arange} function for the $x$-axis from the \texttt{numpy} package.}
        \sitem Plot the function $f(x) = 1 + sin(3x)$ in the range $[0,4]$ labelling the axis and adding a grid.
        \Hint{Use the \texttt{pyplot.subplots} function from the package \texttt{matplotlib} and \texttt{sin} from \texttt{numpy}.}
%https://matplotlib.org/gallery/lines_bars_and_markers/simple_plot.html#sphx-glr-gallery-lines-bars-and-markers-simple-plot-py
    \sitem Plot an histogram with the distribution of births per month from exercise \ref{Dictionaries}.\ref{birthmonth}.
        \Hint{Use the \texttt{pyplot.hist} function from the module \texttt{matplotlib}. The bins argument is a list of bin edges, included first and last, thus thirteen edges for twelve bins.}
        %https://matplotlib.org/gallery/statistics/histogram_features.html
        \sitem Histogram a randomly generated distribution around a gaussian and add a best fit line.
        \Hint{Use the \texttt{np.random.randn} to generate the distribution.}
\end{enumerate}

\end{document}
