#!/usr/bin/python3
from argparse import ArgumentParser
import json

# read arguments
parser = ArgumentParser()
parser.add_argument('filename', help='The name of the file containing the dictionary.')
args = parser.parse_args()

try:
    with open(args.filename, "r") as infile:
        Wort = json.load(infile)
        print("The currently stored dictionary looks like:")
        print(Wort)
except FileNotFoundError as fnf:
    print(fnf)
    quit()
except json.decoder.JSONDecodeError:
    print("The content of the file cannot be decoded into a dictionary (possible syntax error)")
    quit()

# If the user wants to modify the dictionary stored in the file:
while True:
    modify = input("Do you want to modify or add key-value pairs? (Yn)")
    if modify.lower() == "y" or not modify:
        while True:
            key = input("Please enter a key (you can quit by typing 'quit'): ")
            if key == "quit":
                break
            value = input("Please enter a value (you can quit by typing 'quit'): ")
            if value == "quit":
                break
            Wort[key] = value
        with open(args.filename, "w") as outfile:
            json.dump(Wort, outfile)
        print("The newly stored dictionary looks like:")
        print(Wort)
        break
    elif modify.lower() == "n":
        break
    else:
        print("Not a valid choice. Please enter 'y' or 'n'")
