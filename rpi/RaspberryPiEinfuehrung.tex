\documentclass[pdftex,a4paper]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{relsize}
\usepackage[squaren]{SIunits}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{listings}
\usepackage{float}
\usepackage{subfig}
\usepackage[colorlinks=false, pdfborder={0 0 0}]{hyperref}
\usepackage{braket}
\usepackage{MnSymbol}
\usepackage{caption}
\usepackage{wrapfig}
\usepackage{ulem}
\usepackage{color}
\usepackage{microtype}
\usepackage{bytefield}
\usepackage{feynmf}
\usepackage{ngerman}

\pdfminorversion=4

\definecolor{lightgray}{rgb}{0.7,0.7,0.7}


\usepackage[electronic]{ifsym}

\newcommand{\AND}{\wedge}
\newcommand{\OR}{\vee}
\newcommand{\XOR}{\oplus}
\newcommand{\NOT}[1]{\overline{#1}}

\usepackage{dcolumn}
\newcolumntype{d}{D{.}{.}{2.5}}
\newcolumntype{s}{D{.}{.}{1.2}}

%inkscape-svg
\newcommand{\executeiffilenewer}[3]{%
\ifnum\pdfstrcmp{\pdffilemoddate{#1}}%
{\pdffilemoddate{#2}}>0%
{\immediate\write18{#3}}\fi%
}
\newcommand{\includesvg}[1]{%
\executeiffilenewer{#1.svg}{#1.pdf}%
{inkscape -z -D --file=#1.svg %
--export-pdf=#1.pdf --export-latex}%
\input{#1.pdf_tex}%
}
\graphicspath{{./bilder/}}
%

\newcommand{\panda}{$\overline{\text{P}}$ANDA}

\lstset{
	backgroundcolor=\color[rgb]{.9,.9,.9},
	tabsize=3,
	rulecolor=,
	inputencoding=latin1,
	language=VHDL,
        basicstyle=\scriptsize,
        upquote=true,
        aboveskip={1.5\baselineskip},
        columns=fixed,
        showstringspaces=false,
        extendedchars=true,
        breaklines=true,
        prebreak = \raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}},
        frame=single,
        showtabs=false,
        tabsize=3,
        showspaces=false,
        showstringspaces=false,
        identifierstyle=\ttfamily,
        keywordstyle=\color[rgb]{0.05,.337,.65},
        commentstyle=\color[rgb]{1,0.604,0},
        stringstyle=\color[rgb]{0.016,0.208,0.424},
        numbers=left,
		  numberstyle=\tiny\color[rgb]{.5,.5,.5}
}


%https://github.com/mavcunha/checklists/blob/master/input/preamble.inc
\input{checklists/preamble.inc}

\numberwithin{equation}{section}

\title{Einführung in die Elektronik mit dem Raspberry Pi}

\author{Denis Bertini $<$d.bertini@gsi.de$>$,\\Sören Fleischer $<$s.fleischer@gsi.de$>$}

\begin{document}

\maketitle


\newpage

\tableofcontents

\newpage

\section{Grundlagen}

\subsection{LEDs}

\subsubsection{Was ist eine LED?}

Leuchtdioden wandeln elektrische Energie in Licht um.
Sie funktionieren wie Halbleiterdioden, die in Durchlassrichtung Licht erzeugen.
Die Kurzbezeichnung LED ist die Abkürzung für ``Light Emitting Diode'', was auf Deutsch ``Lichtemittierende Diode'' bedeutet.

Leuchtdioden gibt es in verschiedenen Farben, Größen und Bauformen.
Sie werden als Signal und Lichtgeber in unterschiedlichen Bereichen eingesetzt.

Die Leuchtdiode schaltet sehr schnell vom leuchtenden in den nichtleuchtenden Zustand, und umgekehrt.
Der Lichtstrahl kann bis in den MHz-Bereich getaktet werden. Allerdings ist das für das menschliche Auge nur als Leuchtbrei sichtbar.
Die Helligkeit der LED ist dann geringer, als es beim eingestellten Stromfluss zu erwarten wäre.

Die Lebensdauer beträgt sagenhafte $10^6$ Stunden. Im Vergleich zu normalen Lampen, dessen Lebensdauer seit langer Zeit durch das Glühbirnenkartell auf 1000 Stunden künstlich begrenzt wird, ist das sehr lange.
Die gebräuchlichsten Bauformen haben einen Durchmesser von 3 mm oder 5 mm.

\subsubsection{Polung}
Wie jede andere Diode ist auch die LED polungsabhängig (Abbildung \ref{led:polung}).
Die eine Anschlussseite ist die Anode, die andere Seite die Kathode.

Wenn man in die Leuchtdiode hineinschaut, dann ist die dickere Seite die Kathode.
Äußerlich erkennt man die Kathode am kürzeren Anschluss oder an der abgeflachten Seite des Gehäuserandes an der Unterseite.
\begin{figure}[H]
  \begin{center}
    \includegraphics[height=6.5cm]{pics/LED-polarity.pdf}
  \end{center}
  \caption{LED Polung}
  \label{led:polung}
\end{figure}
% https://commons.wikimedia.org/wiki/File:%2B-_of_LED_2.svg

\subsubsection{Farben und Halbleitermaterial}
Die klassischen Farben sind rot, grün, gelb, und orange.
Es gibt aber auch noch blau und weiß.
Je nach Farbe besteht der Halbleiterkristall einer Leuchtdiode aus unterschiedlichen Materialien.
Die Farbe des Lichts bzw. die Wellenlänge des Lichts wird vom Halbleiterkristall und der Dotierung bestimmt.
Der Kristall besteht aus einer n- und einer p-Schicht.
Von daher unterscheidet er sich kaum von einer normalen Halbleiterdiode.

LEDs unterscheiden sich nicht nur in ihrer Farbe, sondern auch in ihren elektrischen Eigenschaften.
Teilweise kann man die Farben nicht untereinander tauschen.
Die Durchlassspannung ist unterschiedlich und stark vom Halbleitermaterial abhängig.

Rote Leuchtdioden $(\lambda = 0.66 \micro \meter)$ haben einen besonders guten Wirkungsgrad.
Den höchsten Wirkungsgrad haben Infrarot-Leuchtdioden $(\lambda = 0.9 \dots 0.94 \micro \meter)$.

Die LED ist je nach Farbe aus unterschiedlichen Mischkristallen aufgebaut:

\begin{itemize}
\item Galliumarsenid (GaAs)
\item Galliumarsenidphosphid (GaAsP)
\item Galliumphosphid (GaP)
\item Aluminium-Indium-Gallium-Phosphid (AlInGaP) für Rot, Rot-Orange, Amber
\item Indium-Gallium-Nitrogen (InGaN) für Grün, Cyan, Blau, Weiß
\item GalliumNitrid (GaN) für Blau
\end{itemize}

\subsubsection{Funktionsweise einer LED}
Eine Leuchtdiode besteht aus einem n-leitenden Grundhalbleiter.
Darauf ist eine sehr dünne p-leitende Halbleiterschicht mit großer Löcherdichte aufgebracht.
Wie bei der normalen Diode wird die Grenzschicht mit freien Ladungsträgern überschwemmt.
Die Elektronen rekombinieren mit den Löchern.
Dabei geben die Elektronen ihre Energie in Form eines Lichtblitzes frei.
Da die p-Schicht sehr dünn ist, kann das Licht entweichen.
Schon bei kleinen Stromstärken ist eine Lichtabstrahlung wahrnehmbar.
Die Lichtstärke wächst proportional mit der Stromstärke.

Da von dem Halbleiterkristall nur eine geringe Lichtstrahlung ausgeht, ist das Metall unter dem Kristall halbkugelförmig.
Dadurch wird das Licht gestreut.
Durch das linsenförmige Gehäuse wird das Licht gebündelt.
So können Leuchtdioden schon mit wenigen Milliampere Strom sehr hell leuchten.

\begin{figure}[H]
  \begin{center}
    \includegraphics[height=5cm]{pics/led_funktion.png}
  \end{center}
  \caption{LED Funktionweise}
  \label{led:funktion}
\end{figure}

\subsubsection{Schaltzeichen}

\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[width=5cm]{pics/led_schaltzeichen.png}
  \end{center}
  \caption{LED Schaltzeichen}
  \label{led:schalt}
\end{figure}

\subsubsection{Standard-LEDs}
Standard-Leuchtdioden haben einen Durchmesser von 5 mm.
Sie sind die häufigsten verwendeten Leuchtdioden in elektronischen Schaltungen.
Sie beginnen bei 8 bis 12~\milli\ampere~zu leuchten.
Erhöht man den Strom, leuchten Sie heller.
Bei 20~\milli\ampere~ist die maximale Leuchtkraft erreicht.
Der Unterschied zu 15~\milli\ampere~ist aber nur minimal.
Meist ist ein Strom von 10~\milli\ampere~schon ausreichend, um sie ausreichend zum Leuchten zu bringen.

Die folgende Tabelle gibt Auskunft über die Durchflussspannung $U_F$.
Über die genaue Durchflussspannung $U_F$ und Durchflussstrom $I_F$ gibt nur das Datenblatt der Leuchtdiode Auskunft (Abbildung \ref{led:leds}).

Vorsicht, je nach Hersteller kann es hier Unterschiede geben.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=5cm]{pics/led_tabelle1.png}
  \end{center}
  \caption{Typische Durchlassspannung von Standard-LEDs}
  \label{led:leds}
\end{figure}


\subsubsection{Low-Current-LEDs}
Low-Current-Leuchtdioden haben einen Durchmesser von 3 oder 5 mm.
Sie leuchten bereits bei 2 mA mit bis zu 5 mcd.
Erhöht man den Strom leuchten Sie heller.
Bei 20~mA ist die maximale Leuchtkraft erreicht.

Low-Current-LEDs haben die Eigenschaft, dass sie bei 2 mA noch leuchten, was Standard-LEDs nicht tun.
Diese hören (je nach Hersteller) schon bei 8 bis 10 mA auf zu leuchten.

Low-Current bedeutet nicht, dass eine LED bei 2 mA genauso hell leuchtet wie bei 20~mA, sondern dass diese LED bis hinter zu 2 mA betrieben werden kann und zumindest schwach leuchtet.

Die folgende Tabelle gibt Auskunft über die Durchflussspannung $U_F$.
Die genaue Durchflussspannung $U_F$ und Durchflussstrom $I_F$ gibt nur das Datenblatt der Leuchtdiode Auskunft.

Vorsicht, je nach Hersteller kann es hier Unterschiede geben.
\begin{figure}[H]
  \begin{center}
    \includegraphics[width=5cm]{pics/led_current.png}
  \end{center}
  \caption{Standard LEDs}
  \label{led:lowcurrent}
\end{figure}

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.5\textwidth]{pics/led_typ.png}
  \end{center}
  \caption{Beispielhaft LEDs mit höherem Durchflussstrom}
  \label{led:highcurrent}
\end{figure}

\subsubsection{Die Leuchtdiode in der Anwendung}
Leuchtdioden reagieren sehr empfindlich auf einen zu großen Durchlassstrom.
Deshalb darf eine Leuchtdiode niemals direkt an eine Spannung angeschlossen werden.

Eine Leuchtdiode muss immer mit einem Vorwiderstand oder einem strombegrenzenden Bauteil beschaltet sein.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=4cm]{./pics/led_u.png}
  \end{center}
  \caption{Typische Beschaltung einer LED}
  \label{led:mitwiderstand}
\end{figure}
Mit einem Vorwiderstand wird der Durchlassstrom $I_F$, der durch die Leuchtdiode fließt, begrenzt.
Bei der Widerstandsbestimmung muss die jeweilige Durchlassspannung $U_F$ berücksichtigt werden.

\begin{align}
R_V =\frac{U_\text{ges}-U_F}{I_F}
\end{align}

Die Formel berechnet den Vorwiderstand $R_V$ über die Gesamtspannung $U_\text{ges}$ abzüglich der Durchlassspannung $U_F$ durch den Durchlassstrom $I_F$.
Eine Leuchtdiode brennt schon bei einem Bruchteil des maximalen Durchlassstroms.
Außerdem müssen Leuchtdioden nicht zwingend mit ihrer vollen Leuchtstärke strahlen.
Meist reichen schon wenige mA aus um eine ausreichende Helligkeit zu erzeugen.

\subsubsection{Warum wird ein Vorwiderstand benötigt?}
LEDs müssen immer mit einem Vorwiderstand betrieben werden.
Das gilt auch dann, wenn eine Betriebsspannung zur Verfügung steht, die der LED-Durchflussspannung entspricht.
Der Vorwiderstand dient zum einen zum Begrenzen der Spannung,
in dem sich die Betriebsspannung zwischen Vorwiderstand und LED aufteilt.
An der LED stellt sich ein fester vorher bekannter Spannungsabfall ein.
Das ist die Durchlassspannung der Leuchtdiode, die allerdings nicht allzu konstant und unter Exemplarstreuung leidet.
Am Vorwiderstand fällt dann noch der Rest der Betriebsspannung ab.
Was aber noch viel wichtiger ist, der Vorwiderstand begrenzt den Strom, der durch die LED fließt.
Der Grund, warum eine Strombegrenzung notwendig ist, ist schnell erkärt.

Eine Leuchtdiode ist kein ohmscher Verbraucher, dessen Widerstand immer gleich ist.
Eine Leuchtdiode ist ein Halbleiter, dessen Widerstand nach Anlegen einer Spannung gegen Null sinkt.
Das bedeutet, der Strom steigt rein theoretisch unendlich an!

Das bedeutet, die Leuchtdiode ist ein sehr stromhungriger Halbleiter.
Doch zu viel Strom verträgt die Leuchtdiode nicht. Zu viel Strom zerstört die Leuchtdiode.
Vor der Zerstörung tritt erst ein Temperaturanstieg ein.
Die Leuchtdiode wird wärmer.
Bekanntlich leiten warme Halbleiter besser als kalte.
Es folgt also ein weiterer Stromanstieg, der dazu führt, dass die LED heiß und letztendlich zerstört wird.
Dieser Effekt muss nicht zwangsläufig und auch nicht sofort eintreten.
Er ist in gewisser Weise davon abhängig, was für eine Spannungsquelle verwendet wird und wie lange die Leuchtdiode daran betrieben wird.
So manch unbedarfte Anwender wird also nie mit diesem Problem konfrontiert werden.
Wer nur mal kurz eine Leuchtdiode ohne Vorwiderstand betreibt, der wird sie dabei nicht gleich zerstören.
Es kann auch sein, dass das mehrere Stunden gutgeht.

\newpage
\subsection{Raspberry Pi}

\subsubsection{Was ist der Raspberry Pi?}
Der Raspberry Pi (abgekürzt RPi oder RasPi) ist ein günstiger Einplatinencomputer in Kreditkartengröße, welcher von der Raspberry Pi Foundation entwickelt wurde.

Die Raspberry Pi Foundation ist eine in Großbritannien als wohltätige Organisation eingetragene Stiftung.

Sie hat sich zum Ziel gesetzt, das Studium der Informatik und verwandten Themen zu fördern, insbesondere im Schulbereich.


\subsubsection{Wie ist der Raspberry Pi aufgebaut?}
\label{rpi_bau}

Der Raspberry Pi existiert mittlerweile in vielen Varianten. In diesem Projekt verwenden wir den Raspberry Pi 3 Model B.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{./pics/gh_rpi3b.png}
	\caption{Eigenschaften RPI3b (gh.de)}
	\label{ghrpi3b}
\end{figure}

Dieses Modell ist eine kontinuierliche Weiterentwicklung des Vorgängermodells Raspberry Pi 2.

Der Raspberry Pi 3 enthält einen ARM Cortex-A53 Quad-Core-Prozessor mit 1,2 GHz und 1 GB LPDDR2 Arbeitsspeicher.

\textbf{WLAN und Bluetooth onboard}\\
Die Besonderheit des Raspberry Pi 3 ist, dass WLAN nach IEEE 802.11 b/g/n im 2,4 GHz-Bereich mit 150 MBit/s und Bluetooth Low Energy
onboard sind und nicht durch externe USB-Adapter nachgerüstet werden müssen.

\textbf{64-Bit-Unterstützung}\\
Beworben wird der Raspberry Pi 3 mit seiner 64-Bit-Unterstützung.
Das ist heute bei modernen CPUs ganz normal. 64 Bit auf der Hardware-Seite muss allerdings vom Betriebssystem und der Software auch unterstützt werden.

Insbesondere muss beachtet werden, dass die CPU keine x64\_64-Architektur hat, sondern eine ARM-Architektur.

Das bedeutet, dass gewöhnliche Linux-Binärdistributionen nicht auf dem Raspberry Pi laufen. Stattdessen müssen entweder spezielle Distributionen für den Raspberry Pi, wie z.B. Raspbian, oder generische ARM-spezifische Distributionen wie Arch Linux ARM, verwendet werden.

Um Binaries eines im Quelltext vorliegenden Programms für den Raspberry Pi zu erzeugen, muss dieser entweder mit einem Crosscompiler oder direkt auf dem Rasbperry Pi kompiliert werden.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=1\textwidth]{pics/rpi_3.png}
  \end{center}
  \caption{Rasberry Pi 3 Model B, mit der Beschriftung der Anschlüsse.}
\label{rpi:model3b}
\end{figure}


\subsection {T-Cobbler und Breadboard}
Mit Hilfe des T-Cobblers (Abbildung \ref{rpi:tcobbler}) sind die GPIO-Pins (General Purpose Input-Output) einfacher zu erreichen.
Die GPIO-Pins sind die universellen digitalen Ein-/Ausgabe-Kanäle.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{pics/tcobbler.png}
  \end{center}
  \caption{T-Cobbler}
\label{rpi:tcobbler}
\end{figure}

Hier die Anordnung der GPIO-Pins:

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=1\textwidth]{pics/gpio.png}
  \end{center}
  \caption{Anordnung der GPIOs}
\label{rpi:tgpio}
\end{figure}


\subsubsection {Steckplatine (Breadboard)}
Auf dem Breadboard werden die Schaltungen dieser Versuche aufgebaut.
Eine Steckplatine (Abbildung \ref{rpi:platine}) besteht auf zwei ``Bus''-Bereichen (Außenseiten) und einem ``Terminal''-Bereich (Mitte). Jede Strip ist eine Kontaktreihe, besteht daher aus verbundenen Pins. Die Bus-Strips (auch Power-Rails genannt) werden für die Spannungsversorgung benutzt: über Jumpers (Steckbrücken) wird Strom von Bus-Strips in Terminal-Bereich geleitet. Plus wird mit rot und Minus mit Blau gekennzeichnet.
Terminal Strips sind Kontaktreihen von horizontal miteinander verbunden Pins (``a'' bis ``e'' und ``f'' bis ``j'') auf denen die eigentliche Schaltung aufgebaut wird. Der Mittelsteg trennt jede Reihe in zwei Strips.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=1\textwidth]{pics/breadboard_with_connections.pdf}
  \end{center}
  \caption{Breadboard. Kontaktreihen sind als grüne Linien gezeigt.}
\label{rpi:platine}
\end{figure}

Der T-Cobbler wird an der linken Seite des Breadboards gesteckt (Abbildung \ref{rpi:tcobbler}).
Anschließend werden die gewünschten Bauteile entsprechend der gewünschten Schaltung auf das Breadboard gesteckt.

\newpage
\subsection{State Machine}
\subsubsection{Was ist eine State Machine?}

Die State Machine ist ein Konzept zur Beschreibung bzw dem Design eines Systems, welches typischerweise in Software oder Hardware implementiert ist.\\

Für den Begriff State Machine gibt es mehrere Synonyme, z.B. Finite State Machine, Endlicher Zustandsautomat, Moore Machine, Moore-Automat.\\

Eine State Machine kann man so definieren\dots

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=.75\textwidth]{./pics/fsm_wikidef.png}
  \end{center}
  \caption{Eine Definition einer Moore-Machine (de.wikipedia.org)}
\label{fsm:wikidef}
\end{figure}

\dots muss man aber nicht.\\

Einfacher verständlich sind die Ideen hinter dem Konzept der State Machine.

\begin{enumerate}
\item Das System hat eine endliche Anzahl von \textbf{Zuständen}. Alle diese Zustände sind bekannt und wohldefiniert.
\item Das System befindet sich zu jeder Zeit in \textbf{genau einem} dieser Zustände.
\item Das System geht von einem Zustand in den nächsten über. Dieser Übergang kann auf \textbf{Eingaben} von außen basieren, muss aber nicht.
\item Die \textbf{Ausgabe} des Systems ist \uline{ausschließlich} vom Zustand abhängig.\footnote{Bei der Mealy Machine ist die Ausgabe stattdessen vom Zustands-Übergang abhängig.}
\end{enumerate}

Der beim Einschalten des Systems angenommene Zustand wird \textbf{Anfangszustand} oder \textbf{Startzustand} genannt.

Ein Zustand, aus dem es kein Entrinnen mehr gibt, wird \textbf{Endzustand} genannt. Davon kann es mehrere geben, oder auch gar keinen.

\subsubsection{Grafische Darstellung von State Machines}

Eine State Machine lässt sich sehr einfach grafisch abbilden und designen.

Dabei wird jeder Zustand als Ellipse gezeichnet. In die Ellipse wird der Name des Zustands geschrieben, sowie die Ausgabe des Zustands.

Die Zustandsübergänge werden als Pfeile vom alten zum neuen Zustand gezeichnet.
Wenn der Übergang nur durch eine bestimmte Eingabe ausgelöst werden soll, wird diese Eingabe an den Pfeil geschrieben.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=.55\textwidth]{./pics/MooreMachineExample.pdf}
  \end{center}
  \caption{Beispiel für die grafische Darstellung einer State Machine (Von Biezl - Eigenes Werk, Gemeinfrei, \url{https://commons.wikimedia.org/w/index.php?curid=11420560})}
\label{fsm:wikifsm}
\end{figure}

Diese State Machine hat die Zustände $q_0$ bis $q_3$ und kann die Ausgaben $a$, $b$, $c$ machen. Als Eingaben akzeptiert sie $x$, $y$, $z$. Der Anfangszustand $q_0$ wird durch den Pfeil aus dem Nichts angezeigt. Einen Endzustand gibt es nicht, da von jedem Zustand aus ein Pfeil zu einem anderen Zustand existiert.

\subsubsection{Implementierungs-Tipps für State Machines}

Eine State Machine lässt sich erfahrungsgemäß gut implementieren, indem man eine \textbf{State-Variable} und eine \textbf{Next-State-Variable} definiert. Im Hauptteil des Programms werden dann mit einem großen if/elseif/else alle möglichen States durchgegangen.\\

In \textbf{jeder} State-Möglichkeit wird dann folgendes gemacht:
\begin{enumerate}
\item Es muss die \textbf{gesamte Ausgabe} definiert werden, am besten gleich am Anfang.

Hier ist wichtig, dass die Ausgabe erfolgt, ohne zuvor durch irgendwelche Kontrollfluss-Konstrukte wie ``if'' gegangen zu sein, sonst hat man die Regel verletzt, dass die Ausgabe nur vom Zustand abhängen darf. Falls man sich in der Lage wiederfindet, bei einem Zustand die Ausgabe von anderen Dingen abhängen lassen zu wollen, sollte man diesen Zustand in mehrere Zustände aufsplitten, so dass jeder Zustand immer eine exakt bekannte, konstante, Ausgabe hat.

Noch viel wichtiger ist, dass die \textit{gesamte} Ausgabe definiert wird. Macht man das nicht, hat man sich einen Latch gebaut, also ein System mit  Vorzustandsabhängigkeit. So etwas kann zu selten auftretenden Fehlern führen und schwierig zu debuggen sein, daher ist das Bauen eines Latches dringend zu vermeiden.

\item Es muss der Folgezustand ausgerechnet werden und in die Next-State-Variable geschrieben werden.

Auch hier sollte man bei der Ablauflogik dafür sorgen, dass der Folgezustand \textbf{in jedem Fall} explizit ausgerechnet wird.
\end{enumerate}

Ganz unten, unter dem großen if, wird dann der Wert der Next-State-Variable in die State-Variable geschrieben.\\

Anschließend beginnt das Spiel erneut und das große if wird durchlaufen.

\subsubsection{Vorteile und Grenzen von State Machines}
\begin{enumerate}
\item Eine ordentlich implementierte State Machine ist vom Programmcode her sehr übersichtlich, leicht verständlich, und damit auch zukünftig oder von anderen Leuten wartbar und ggfs erweiterbar.
\item Dadurch ist bei Fehlfunktionen die fehlerhafte Codestelle leicht auffindbar und korrigierbar.
\item Das Design eines Systems als State Machine erleichtert es, das gesamte Verhalten voll durchzudefinieren. Das versehentliche Vergessen skurriler Situationen wird erschwert.
\item Dadurch ist die Wahrscheinlichkeit verringert, dass man undefiniertes Verhalten implementiert.
\end{enumerate}

State Machines sind jedoch kein Allheilmittel, mit dem sich sämtliche Probleme lösen lassen.
So sind sie für kontinuierliche Berechnungen z.B. bei Physik-Simulationen oder Regelkreisen tendenziell ungeeignet.\\

Bei diskreten Systemen, z.B. im Bereich Slow Control oder zur Implementierung von zustandsbehafteten Protokollen sind State Machines jedoch von unschätzbarem Wert.

\newpage
\section{Aufgaben}

\subsection{Checkliste}

Bitte überprüfen Sie vor der Inbetriebnahme folgende Items.\\

\begin{checklist}{Before Start}
  \item{SD-Karte}{Eingelegt}
  \item{Tastatur (USB)}{Eingesteckt}
  \item{Maus (USB)}{Eingesteckt}
  \item{Monitor (HDMI)}{Eingesteckt (beide Enden)}
  \item{T-Cobbler-Flachbandkabel}{Eingesteckt (beide Enden)}
  \item{T-Cobbler auf Breadboard}{Eingesteckt}
\end{checklist}

Danach können Sie die Stromversorgung anschließen. Die Status-LEDs des RPi sollten dann angehen und der RPi sollte über den Monitor sichtbar starten.\\

Um mit dem RPi zu interagieren, werden wir die grafische Oberfläche, aber dort vorwiegend das Terminal, verwenden.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{pics/terminal.png}
  \end{center}
  \caption{Grafische Oberfäche}
\label{rpi:terminal}
\end{figure}

\subsection{Aufgabe 1}

\subsubsection{Erste Schritte}
Öffnen Sie ein Terminal und führen Sie folgende Befehle aus:

\begin{verbatim}
$ gpio -v
$ gpio readall
\end{verbatim}

\begin{enumerate}
\item Welche Raspberry-Pi-Version haben wir?
\item An welcher Hardware-Pin-Nummer ist GPIO-Pin 17 angeschlossen?
\end{enumerate}

\subsubsection{LED direkt anschließen}
Bitte trennen Sie den T-Cobbler vom Breadboard.

Als erstes wollen wir eine LED direkt an 3.3 V mit ca 10 mA betreiben.

\begin{enumerate}
\item Bitte berechnen Sie den nötigen Vorwiderstand.
\item Was tun, wenn kein Widerstand mit genau diesem Wert verfügbar ist?
\item Bitte entwerfen Sie eine Schaltung auf dem Breadboard und zeigen diese zunächst dem Betreuer.
\end{enumerate}

\subsubsection{LED an GPIO anschließen und blinken lassen}

Jetzt wollen wir ein Python-Script schreiben, der die LED an- und ausschaltet.
Um den Status der GPIO-Pins auslesen und verändern zu können, benutzen wir das Python-Modul \texttt{RPi.GPIO}. 
Dafür ist es notwendig, zu Beginn des Python-Scripts zu entscheiden, welche Pin-Nummerierung man gerne hätte.
Mit 
\begin{lstlisting}[language=Python, numbers=none]
GPIO.setmode(GPIO.BCM)
\end{lstlisting}
werden die Pins im Code mit ihren GPIO-Nummern verwiesen, während mit
\begin{lstlisting}[language=Python, numbers=none]
GPIO.setmode(GPIO.BOARD)
\end{lstlisting}
werden sie mit ihren physikalischen Nummern verwiesen.

Dann muss man jene Pins, die man steuern möchte, als Output-Pins festlegen. Idealerweise gibt man dann direkt den Anfangswert des Pins an. Wählbar sind \lstinline{GPIO.HIGH} und \lstinline{GPIO.LOW}.
\begin{lstlisting}[language=Python, numbers=none]
GPIO.setup(ledPin, GPIO.OUT, initial=GPIO.LOW)
\end{lstlisting}
~\\
Um später den Wert des Pins zu ändern, nutzt man folgenden Befehl:
\begin{lstlisting}[language=Python, numbers=none]
GPIO.output(ledPin, GPIO.HIGH)
GPIO.output(ledPin, GPIO.LOW)
\end{lstlisting}

Am Ende des Programms sollte immer folgendes aufgerufen werden, um die Pin-Nutzungs-Zustände wieder zurückzusetzen:
\begin{lstlisting}[language=Python, numbers=none]
GPIO.cleanup()
\end{lstlisting}

\begin{enumerate}
\item Bitte schließen Sie jetzt den positiven Pin der LED an einen der GPIO-Pins an.
\item Bitte schreiben Sie ein Python-Script, welches die LED im Sekundentakt an- und ausschaltet.
\end{enumerate}


\subsubsection{Mikrotaster anschließen und auslesen}
Um während des Programmablaufs Eingaben machen können, wollen wir einen der bereitliegenden Taster verwenden.

\begin{enumerate}
\item Bitte finden Sie heraus, wie die 4 Pins des Tasters miteinander verbunden sind.
\item Bitte überlegen Sie sich eine Schaltung, mit der man den Zustand des Tasters (offen/geschlossen) mit dem RPi auslesen könnte. Tipp: Wir wollen keine Kurzschlüsse auf dem Breadboard oder im RPi. Zeigen Sie diese Schaltungsidee bitte zunächst dem Betreuer.
\item Bauen Sie bitte die Schaltung auf dem Breadboard auf.
\item Bitte schreiben Sie ein Programm, welches bei Änderung des Taster-Zustands den neuen Zustand ausgibt.
\end{enumerate}

Um einen GPIO-Pin zum Lesen zu benutzen, initialisieren wir ihn folgendermaßen:
\begin{lstlisting}[language=Python, numbers=none]
GPIO.setup(buttonPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
\end{lstlisting}


\subsubsection{Mit Mikrotaster LED umschalten}
Bitte schreiben Sie nun je ein Programm, welches
\begin{enumerate}
\item Beim Drücken des Tasters den Leucht-Zustand der LED umschaltet (toggle)
\item Erst beim Drücken und wieder Loslassen des Tasters den Leucht-Zustand der LED umschaltet (toggle)
\item Die LED dann umschaltet (toggle), wenn der Taster 2 Sekunden lang gedrückt gehalten wird.
\end{enumerate}

\newpage
\subsection{Aufgabe 2}
Bitte bauen Sie eine Schaltung für zwei Ampeln (mit je rot, gelb, grün) und einer Kontaktschwelle (hier: Taster) auf dem Breadboard auf und schreiben Sie ein Programm, welches eine Ampelschaltung mit folgenden Eigenschaften realisiert:

\begin{enumerate}
\item Die Hauptstraße hat standardmäßig grün.
\item Die Grünphase der Hauptstraße dauert mindestens 10 Sekunden.
\item Nach Ablauf dieser Mindestgründauer soll die Schaltung schnell (max 0.1 Sekunden) auf eine etwaige Grün-Anforderung der untergeordneten Straße reagieren.
\item Die untergeordnete Straße hat eine Kontaktschwelle, mit welcher eine Grünphase für sie angefordert wird.
\item Die Gelbphase dauert 2 Sekunden.
\item Die Rotgelbphase dauert 1 Sekunde.
\item Die gemeinsame Rotphase dauert 5 Sekunden.
\end{enumerate}

\end{document}
